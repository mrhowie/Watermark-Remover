﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WatermarkRemoverGUI
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Remove Watermark
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button1_Click(object sender, EventArgs e)
        {
            Registry.SetValue(@"HKEY_LOCAL_MACHINE\Software\microsoft\windows nt\currentversion\softwareprotectionplatform", "SkipRearm",
                1, RegistryValueKind.DWord);
            Registry.SetValue(@"HKEY_LOCAL_MACHINE\Software\microsoft\windows nt\currentversion\softwareprotectionplatform\Activation",
                "Manual", 1, RegistryValueKind.DWord);
            Registry.SetValue(@"HKEY_LOCAL_MACHINE\System\CurrentControlSet\Services\svsvc", "Start",
                4, RegistryValueKind.DWord);
            label1.Text = ("Watermark Removed! You should now restart your PC.");
        }
        /// <summary>
        /// Set Autostart
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Button2_Click(object sender, EventArgs e)
        {
            RegistryKey rk = Registry.CurrentUser.OpenSubKey
            ("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);
            string PathToHere = Application.ExecutablePath;
            string NewPath = (System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal) + "\\watermark.exe");
            File.Copy(PathToHere, NewPath, true);
            rk.SetValue("WatermarkRemover", NewPath);
            label1.Text = ("Watermark will now autoremove.");
        }
    }
}
